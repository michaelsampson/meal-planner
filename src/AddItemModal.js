import React, { Component } from 'react';
import './App.css';
import styled from 'styled-components';
import Modal from 'react-modal';

import ManualItemEntry from './ManualItemEntry';
import Search from './Search';
import ScanUPC from './ScanUPC';
import {
  Button
} from './styledComponents';

const Tabs = styled.div`
  width: 100vw;
  max-width: 30rem;
  padding: 1rem;
`;

const OpenModalButton = styled(Button)`
  margin-right: 1rem;
  margin-bottom: 1rem;
`

const TabContent = styled.div`
  border: 0.1rem solid black;
  border-radius: 0.4rem;
  padding: 1rem;
`;

const TabButtons = styled.div`
  display: flex;
`;

const TabButton = styled.div`
  cursor: pointer;
  padding: 1rem;
  border-top: 0.1rem solid black;
  border-bottom: 0.1rem solid black;
  border-left: 0.1rem solid black;
  border-right: 0.1rem solid black;
  border-top-left-radius: 0.3rem;
  border-top-right-radius: 0.3rem;
  margin-left: 1rem;
  position: relative;
  top: 0.1rem;
  background-color: white;
`;

const ActiveTabButton = styled(TabButton)`
  border-bottom: none;
`;

const CloseModalButton = styled.button`
  float: right;
`;

const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

const Tab = ({label, tab, activeTab, setTab}) => {
  if (tab === activeTab) {
    return (
      <ActiveTabButton onClick={() => setTab(tab)}>{label}</ActiveTabButton>
    );
  }

  return (
    <TabButton onClick={() => setTab(tab)}>{label}</TabButton>
  );
};


class EditMeal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tab: 'SEARCH'
    };
  }

  openModal = () => {
    this.setState({modalIsOpen: true});
  }

  closeModal = () => {
    this.setState({modalIsOpen: false});
  }

  onSelectItem = (item) => {
    this.props.onSelectItem(item);
    this.closeModal();
  }

  setTab = (tab) => {
    this.setState({tab});
  }

  render() {
    return (
      <span>
        <OpenModalButton onClick={this.openModal}>Add Item</OpenModalButton>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          contentLabel="Add Item Modal"
          ariaHideApp={false}
          style={customStyles}
        >
          <div>
            <CloseModalButton onClick={this.closeModal} >X</CloseModalButton>
            <Tabs>
              <TabButtons>
                <Tab label="Search" tab="SEARCH" activeTab={this.state.tab} setTab={this.setTab} />
                <Tab label="Manual Entry" tab="MANUAL" activeTab={this.state.tab} setTab={this.setTab} />
                <Tab label="Scan" tab="SCAN" activeTab={this.state.tab} setTab={this.setTab} />
              </TabButtons>
              <TabContent>
                { this.state.tab === 'SEARCH' &&
                  <Search foodGroups={this.props.foodGroups} onSelect={this.onSelectItem}/>
                }
                { this.state.tab === 'MANUAL' &&
                  <ManualItemEntry onSelect={this.onSelectItem} />
                }
                { this.state.tab === 'SCAN' &&
                  <ScanUPC onSelect={this.onSelectItem} />
                }
              </TabContent>
            </Tabs>
          </div>
        </Modal>

      </span>
    );
  }
}

export default EditMeal;
