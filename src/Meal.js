import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import styled from 'styled-components';

import {
  Table,
  Input
} from './styledComponents';

const MealWrapper = styled.div`
  width: 100%;
`;

const ServingsInput = styled(Input)`
  width: 6rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

const MealTable = styled(Table)`
  margin: 3em 0;
`;

const NameTd = styled.td`
  word-break: break-word;
`;

class Meal extends Component {
  state = {
    servings: 1
  }

  getTotal = (nutrient) => {
    return _.reduce(this.props.meal.items, (sum, foodItem) => {
      return ((foodItem.count * foodItem.measures[foodItem.units][nutrient]) * this.state.servings) + sum;
    }, 0);
  }

  handleServingsChange = (event) => {
    this.setState({servings: event.target.value});
  }

  render() {
    return (
      <MealWrapper>
        <MealTable className="details">
          <thead>
            <tr>
              <th colSpan="7">
                <Link to={`/edit/${this.props.meal.id}`}>{this.props.meal.name || '___'}</Link>
              </th>
            </tr>
            <tr>
              <th colSpan="7">
                Tags:
                {
                  _.join(_.map(this.props.meal.tags, (tag) => tag.text), ', ')
                }
              </th>
            </tr>
            <tr>
              <th colSpan="7">
                Servings: <ServingsInput min="0" type="number" value={this.state.servings} onChange={this.handleServingsChange} />
              </th>
            </tr>
            <tr>
              <th>
                Name
              </th>
              <th>
                Units
              </th>
              <th>
                Qty
              </th>
              <th>
                Cal
              </th>
              <th>
                F
              </th>
              <th>
                C
              </th>
              <th>
                P
              </th>
            </tr>
          </thead>
          <tbody>
            {
              _.map(this.props.meal.items, (foodItem, index) => {
                return (
                  <tr key={index}>
                    <NameTd>{foodItem.name}</NameTd>
                    <td>{foodItem.units}</td>
                    <td>{_.round(foodItem.count * this.state.servings)}</td>
                    <td>{_.round(foodItem.measures[foodItem.units].calories * foodItem.count * this.state.servings)}</td>
                    <td>{_.round(foodItem.measures[foodItem.units].fat * foodItem.count * this.state.servings)}</td>
                    <td>{_.round(foodItem.measures[foodItem.units].carbohydrates * foodItem.count * this.state.servings)}</td>
                    <td>{_.round(foodItem.measures[foodItem.units].protein * foodItem.count * this.state.servings)}</td>
                  </tr>
                )
              })
            }
          </tbody>
          <tfoot>
              <tr>
                <td>
                  Total
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                  {_.round(this.getTotal('calories'))}
                </td>
                <td>
                  {_.round(this.getTotal('fat'))}
                </td>
                <td>
                  {_.round(this.getTotal('carbohydrates'))}
                </td>
                <td>
                  {_.round(this.getTotal('protein'))}
                </td>
              </tr>
            </tfoot>
        </MealTable>
      </MealWrapper>
    );
  }
}

export default Meal;
