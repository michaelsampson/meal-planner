import React, { Component } from 'react';
import _ from 'lodash';
import './App.css';
import { fetchWrapper } from './api';
import styled from 'styled-components';
import uuidv4 from 'uuid/v4';

import {
  Select,
  Input,
  Button
} from './styledComponents';

const SearchWrapper = styled.div`
  height: 30rem;
`;

const SearchResult = styled.div`
  border: 0.1em solid black;
  border-radius: 0.4em;
  padding: 1em;
  margin: 1em 0;
  cursor: pointer;
`;

const GroupSelect = styled(Select)`
  width: 20.3rem;
`;

const SearchInput = styled(Input)`
  width: 14rem;
  margin-top: 1rem;
  margin-right: 1rem;
  padding: 1rem 0.5rem;
`;

const SearchResults = styled.div`
  overflow: auto;
  height: 22.5rem;
`;

const API_KEY = 'FzYugfLhT6MkSzisA5mGeB0JpQiryNMzPv7N4mqo';

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      search: '',
      results: [],
      selectedGroupId: []
    }
  }

  getGramNutrientCountFromResults = (results, dataType) => {
    const data = _.find(_.get(results, 'report.food.nutrients', {}), {name: dataType});
    const valuePer100Grams = _.get(data, 'value', 0);
    return valuePer100Grams / 100;
  }

  getNutrientCountFromMeasure = (results, measureLabel, dataType) => {
    const data = _.find(_.get(results, 'report.food.nutrients', {}), {name: dataType});
    const measureData = _.find(data.measures, {label: measureLabel});
    if(!measureData) {
      return 0;
    }
    return _.toNumber(measureData.value) / measureData.qty;
  }


  fetchDetails = async (ndbno) => {
    const url = `https://api.nal.usda.gov/ndb/reports/?ndbno=${ndbno}&type=b&format=json&api_key=${API_KEY}`
    const results = await fetchWrapper(url, 'GET');
    console.log(results);

    const newFoodItem = {
      id: uuidv4(),
      ndbno: ndbno,
      name: _.get(results, 'report.food.name', ''),
      measures: {
        g: {
          calories: this.getGramNutrientCountFromResults(results, 'Energy'),
          fat: this.getGramNutrientCountFromResults(results, 'Total lipid (fat)'),
          carbohydrates: this.getGramNutrientCountFromResults(results, 'Carbohydrate, by difference'),
          protein: this.getGramNutrientCountFromResults(results, 'Protein')
        },
      },
      units: 'g',
      count: 100
    };

    _.each(results.report.food.nutrients[0].measures, (measure) => {
      if(measure) {
        newFoodItem.measures[this.cleanMeasureLable(measure.label)] = {
          calories: this.getNutrientCountFromMeasure(results, measure.label, 'Energy'),
          fat: this.getNutrientCountFromMeasure(results, measure.label, 'Total lipid (fat)'),
          carbohydrates: this.getNutrientCountFromMeasure(results, measure.label, 'Carbohydrate, by difference'),
          protein: this.getNutrientCountFromMeasure(results, measure.label, 'Protein')
        };
      }
    });

    this.props.onSelect(newFoodItem);
    this.setState({results: [], search: ''});
  };

  cleanMeasureLable = (measureLabel) => {
    return _.replace(measureLabel, '/', ' per ');
  }

  onGroupSelect = (event) => {
    this.setState({selectedGroupId: event.target.value});
  }

  handleSearchChange = (event) => {
    this.setState({search: event.target.value});
  }

  search = async () => {
    let url = `https://api.nal.usda.gov/ndb/search/?sort=r&format=json&q=${this.state.search}&max=500&offset=0&api_key=${API_KEY}`;
    if(this.state.selectedGroupId !== 'ALL') {
      url = url + `&fg=${this.state.selectedGroupId}`;
    }
    const results = await fetchWrapper(url, 'GET')
    this.setState({results: _.get(results, 'list.item', [])});
  }


  render() {
    return (
      <SearchWrapper>
        <div>
          <GroupSelect onChange={this.onGroupSelect}>
            {
              _.map(this.props.foodGroups, (group) => {
                return (
                  <option key={group.id} value={group.id}>
                    {group.name}
                  </option>
                );
              })
            }
          </GroupSelect>
        </div>
        <div>
          <SearchInput type="text" name="search" value={this.state.search} onChange={this.handleSearchChange} />
          <Button onClick={this.search}>Search</Button>
        </div>
        <SearchResults>
          {
            _.map(this.state.results, (result) => {
              return (
                <SearchResult key={result.ndbno} onClick={() => this.fetchDetails(result.ndbno)}>
                  <div>{result.name}</div>
                </SearchResult>
              )
            })
          }
        </SearchResults>
      </SearchWrapper>
    );
  }
}

export default Search;
