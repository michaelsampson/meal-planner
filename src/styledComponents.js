import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Input = styled.input`
  border: 0.1rem solid black;
  border-radius: 0.4rem;
  padding: 0.75rem 0.5rem;
`;

export const Select = styled.select`
  color: black;
  background-color: white;
  border: 0.1rem solid black;
  padding: 1rem 0.5rem;
  border-radius: 0.4rem;
  appearance: none;
`;

export const Label = styled.label`
  font-weight: bold;
`;

export const SubmitButton = styled.input`
  border: 0.1rem solid black;
  cursor: pointer;
  border-radius: 0.4rem;
  padding: 1rem;
  background-color: white;
`;

export const Button = styled.button`
  border: 0.1rem solid black;
  cursor: pointer;
  border-radius: 0.4rem;
  padding: 1rem;
  background-color: white;
`;

export const ButtonLink = styled(Link)`
  text-decoration: none;
  color: black;
  border: 0.1rem solid black;
  cursor: pointer;
  border-radius: 0.4rem;
  padding: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
`;

export const Table = styled.table`
  border: 0.1rem solid black;
  border-radius: 0.4rem;
  border-spacing: 0;
  width: 100%;

  td, th {
    border-bottom: 0.1rem solid black;
    padding: 0.5rem 0.5rem 0.5rem 0.5rem;
  }

  tr:last-child > td {
    border-bottom: none;
  }

  td:last-child {
    padding-right: 0.5rem;
  }

  tfoot tr td {
    font-weight: bold;
    border-top: 0.1rem solid black;
  }
`;
