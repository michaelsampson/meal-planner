import React, { Component } from 'react';
import { auth } from './firebase.js';
import { Link, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import {
  Input,
  Label,
  SubmitButton
} from './styledComponents';

const SignUpWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const SignUpButton = styled(SubmitButton)`
  margin-top: 1rem;
`;

const InputLabel = styled(Label)`
  margin-top: 1rem;
  display: block;
`;

const LoginForm = styled.form`
  margin-bottom: 1rem;'
`;

const LoginInput = styled(Input)`
  width: 22rem;
`;

class SignUp extends Component {
  state = {
    email: '',
    password: '',
    passwordConfirm: '',
    error: null
  }

  createUser = (event) => {
    event.preventDefault();
    const {email, password, passwordConfirm} = this.state;
    if(password === passwordConfirm) {
      auth.createUserWithEmailAndPassword(email, password).then(() => {
        this.props.history.push('/login');
      }).catch((error) => {
        console.error(error);
        this.setState({error});
      });
    } else {
      console.error("Password's must match.");
      this.setState({error: "Passwords must match."});
    }
  }

  handleChange = (name, event) => {
    this.setState({[name]: event.target.value});
  }

  render() {
    return (
      <SignUpWrapper>
        <LoginForm>
          <div>
            <InputLabel>Email:</InputLabel>
            <LoginInput type="text" name="email" value={this.state.email} onChange={(e) => this.handleChange('email', e)} />
          </div>
          <div>
            <InputLabel>Password:</InputLabel>
            <LoginInput type="password" value={this.state.password} onChange={(e) => this.handleChange('password', e)} />
          </div>
          <div>
            <InputLabel>Password Confirmation:</InputLabel>
            <LoginInput type="password" value={this.state.passwordConfirm} onChange={(e) => this.handleChange('passwordConfirm', e)} />
          </div>
          { this.state.error && <div>{this.state.error}</div>}
          <SignUpButton type="submit" value="Submit" onClick={this.createUser} />
        </LoginForm>
        <Link to="/login">Login</Link>
      </SignUpWrapper>
    );
  }
}

export default withRouter(SignUp);
