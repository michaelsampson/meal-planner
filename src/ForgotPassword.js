import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { auth } from './firebase.js';
import styled from 'styled-components';
import { toast } from 'react-toastify';

import {
  Input,
  Label,
  SubmitButton
} from './styledComponents';

const ForgotPasswordWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const LoginButton = styled(SubmitButton)`
  margin-top: 1rem;
`;

const InputLabel = styled(Label)`
  margin-top: 1rem;
  display: block;
`;

const LoginForm = styled.form`
  margin-bottom: 1rem;
`;

const LoginInput = styled(Input)`
  width: 22rem;
`;

const actionCodeSettings = {
  url: 'https://simplemealprep.online'
};

class ForgotPassword extends Component {
  state = {
    email: '',
    error: null
  }

  handleChange = (name, event) => {
    this.setState({[name]: event.target.value});
  }

  resetPassword = (event) => {
    event.preventDefault();
    auth.sendPasswordResetEmail(this.state.email, actionCodeSettings).then(() => {
      toast.success("Password reset email sent");
      this.props.history.push('/login');
    }).catch((error) => {
      this.setState({error: error.message});
      toast.error("Error sending reset email");
    });
  }

  render() {
    return (
      <ForgotPasswordWrapper>
        <LoginForm>
          <div>
            <InputLabel>Email:</InputLabel>
            <LoginInput type="text" value={this.state.email} onChange={(e) => this.handleChange('email', e)} />
          </div>
          { this.state.error && <div>{this.state.error}</div>}
          <div>
            <LoginButton type="submit" value="Send Password Reset Email" onClick={this.resetPassword} />
          </div>
        </LoginForm>
        <Link to="/login">Back</Link>
      </ForgotPasswordWrapper>
    );
  }
}

export default withRouter(ForgotPassword);
