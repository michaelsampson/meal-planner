import firebase from 'firebase'
const config = {
  apiKey: "AIzaSyBf5V3M58lVFbG5vwehjgU27MYie5q9J1s",
  authDomain: "meal-planner-2bf22.firebaseapp.com",
  databaseURL: "https://meal-planner-2bf22.firebaseio.com",
  projectId: "meal-planner-2bf22",
  storageBucket: "meal-planner-2bf22.appspot.com",
  messagingSenderId: "458974106500"
};
firebase.initializeApp(config);
export const auth = firebase.auth();
export default firebase;
