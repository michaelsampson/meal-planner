import React, { Component } from 'react';
import styled from 'styled-components';
import uuidv4 from 'uuid/v4';

import {
  Select,
  Input,
  Button,
  Label
} from './styledComponents';


const ManualItemEntryWrapper = styled.div`
  height: 30rem;
`;

const NutrientNameInput = styled(Input)`
  width: 15rem;
`;

const NutrientWrapper = styled.div`
  margin-bottom: 1em;
`;

const NutrientInput = styled(Input)`
  width: 15rem;
`;

const UnitSelect = styled(Select)`
  width: 15rem;
`;

const NutrientLabel = styled(Label)`
  display: inline-block;
  width: 5rem;
`;

class ManualItemEntry extends Component {
  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }

  getInitialState = () => {
    return {
      name: '',
      calories: '',
      fat: '',
      carbohydrates: '',
      protein: '',
      units: 'g',
      count: 1
    };
  }

  handleChange = (name, value) => {
    this.setState({[name]: value});
  }

  addItem = () => {
    const newItem = {
      id: uuidv4(),
      name: this.state.name,
      units: this.state.units,
      count: this.state.count,
      measures: {
        [this.state.units]: {
          calories: this.state.calories / this.state.count,
          fat: this.state.fat / this.state.count,
          carbohydrates: this.state.carbohydrates / this.state.count,
          protein: this.state.protein / this.state.count
        }
      }
    };
    this.props.onSelect(newItem);
    this.setState(this.getInitialState());
  }

  render() {
    return (
      <ManualItemEntryWrapper>
        <NutrientWrapper>
          <NutrientLabel>Name: </NutrientLabel>
          <NutrientNameInput type="text" value={this.state.name} onChange={(e) => this.handleChange('name', e.target.value)} />
        </NutrientWrapper>
        <NutrientWrapper>
          <NutrientLabel>Quantity: </NutrientLabel>
          <NutrientNameInput type="number" value={this.state.count} onChange={(e) => this.handleChange('count', e.target.value)} />
        </NutrientWrapper>
        <NutrientWrapper>
          <NutrientLabel>Units: </NutrientLabel>
          <UnitSelect value={this.state.units} onChange={(e) => this.handleChange('units', e.target.value)}>
            <option value="g">g</option>
            <option value="oz">oz</option>
            <option value="fl oz">fl oz</option>
            <option value="ml">ml</option>
            <option value="cups">cups</option>
            <option value="portion">portion</option>
          </UnitSelect>
        </NutrientWrapper>
        <NutrientWrapper>
          <NutrientLabel>Cals: </NutrientLabel>
          <NutrientInput type="number" value={this.state.calories} onChange={(e) => this.handleChange('calories', e.target.value)} />
        </NutrientWrapper>
        <NutrientWrapper>
          <NutrientLabel>Fat: </NutrientLabel>
          <NutrientInput type="number" value={this.state.fat} onChange={(e) => this.handleChange('fat', e.target.value)} />
        </NutrientWrapper>
        <NutrientWrapper>
          <NutrientLabel>Carbs: </NutrientLabel>
          <NutrientInput type="number" value={this.state.carbohydrates} onChange={(e) => this.handleChange('carbohydrates', e.target.value)} />
        </NutrientWrapper>
        <NutrientWrapper>
          <NutrientLabel>Protein: </NutrientLabel>
          <NutrientInput type="number" value={this.state.protein} onChange={(e) => this.handleChange('protein', e.target.value)} />
        </NutrientWrapper>
        <Button onClick={() => this.addItem()}>Add To Meal</Button>
      </ManualItemEntryWrapper>
    );
  }
}

export default ManualItemEntry;
