import React, { Component } from 'react';
import styled from 'styled-components';
import Quagga from 'quagga';


const ScannerViewport = styled.div`
  width: 100%;
  height: 30rem;
  overflow: hidden;
`;

class BarcodeScanner extends Component {

  componentDidMount() {
    Quagga.init({
      inputStream : {
        name : "Live",
        type : "LiveStream",
        target: document.querySelector('#scanner-viewport'),
        // constraints: {
        //   width: 1024,
        //   height: 800,
        //   facingMode: "environment"
        // },
      },
      decoder : {
        readers : ["upc_reader"],
        multiple: false,
        debug: {
          showCanvas: true,
          showPatches: true,
          showFoundPatches: true,
          showSkeleton: true,
          showLabels: true,
          showPatchLabels: true,
          showRemainingPatchLabels: true,
          boxFromPatches: {
              showTransformed: true,
              showTransformedBox: true,
              showBB: true
          }
        }
      },
      locate: true
    }, (err) => {
        if (err) {
            console.log(err);
            return
        }
        Quagga.start();
    });


    Quagga.onDetected(this.props.onScan);
    this.Quagga = Quagga;
  }

  componentWillUnmount() {
    this.Quagga.stop();
  }

  render() {
    return (
      <ScannerViewport id="scanner-viewport"></ScannerViewport>
    );
  }
}

export default BarcodeScanner;
