import React, { Component } from 'react';
import _ from 'lodash';
import './App.css';
import firebase from './firebase.js';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';
import { auth } from './firebase.js';

import Meal from './Meal';

import {
  Input,
  Label,
  Button,
  ButtonLink,
  Select
} from './styledComponents';

const MealsWrapper = styled.div`
  width: 100%;
`;

const Header = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  border-bottom: 0.1em solid black;
  padding-bottom: 1em;
  margin-bottom: 1em;
`;

const SearchInput = styled(Input)`
  width: 100%;
  margin-bottom: 1rem;
`;

const SortSelect = styled(Select)`
  width: 100%;
`;

const LogoutButton = styled(Button)`
`;

class Meals extends Component {

  state = {
    meals: [],
    search: '',
    sortBy: 'lastViewedTimestamp'
  }

  componentDidMount() {
    const mealsRef = firebase.database().ref(`users/${this.props.userId}/meals`);
    mealsRef.once('value', (snapshot) => {
      const meals = _.map(snapshot.val(), (meal, id) => {
        return {...meal, id};
      });

      this.setState({meals: _.reverse(meals)});
    });
  }

  handleSearchChange = (event) => {
    this.setState({search: event.target.value});
  }

  logout = () => {
    auth.signOut().then(() => {
      this.props.history.push('/login');
    }).catch((error) => {
      console.error(error);
    });
  }

  onSortSelect = (event) => {
    this.setState({sortBy: event.target.value});
  }

  render() {
    let meals = this.state.meals;
    if(this.state.search) {
      meals = _.filter(meals, (meal) => {
        if(_.includes(_.toLower(meal.name), _.toLower(this.state.search))) {
          return true;
        }
        return !!_.find(meal.tags, (tag) => {
          return _.includes(_.toLower(tag.text), _.toLower(this.state.search));
        });
      });
    }

    const sortDirection = this.state.sortBy === 'name' ? 'asc' : 'desc';
    meals = _.orderBy(meals, [this.state.sortBy], [sortDirection]);

    return (
      <MealsWrapper>
        <Header>
          <ButtonLink to="/new">New Meal</ButtonLink>
          <ButtonLink to="/updatePassword">Update Password</ButtonLink>
          <LogoutButton onClick={this.logout}>Logout</LogoutButton>
        </Header>
        <div>
          <Label>Search: </Label>
          <SearchInput type="text" value={this.state.search} onChange={this.handleSearchChange}/>
        </div>
        <div>
          <Label>Sort: </Label>
          <SortSelect onChange={this.onSortSelect} value={this.state.sortBy}>
            <option value="name">Name</option>
            <option value="lastViewedTimestamp">Last Viewed</option>
            <option value="lastUpdatedTimestamp">Last Updated</option>
            <option value="createdTimestamp">Created</option>
          </SortSelect>
        </div>
        {
          _.map(meals, (meal) => {
            return (
              <Meal meal={meal} key={meal.id}/>
            );
          })
        }
      </MealsWrapper>
    );
  }
}

export default withRouter(Meals);
