import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import firebase, { auth } from './firebase.js';
import styled from 'styled-components';
import { toast } from 'react-toastify';

import {
  Input,
  Label,
  SubmitButton
} from './styledComponents';

const UpdatePasswordWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const LoginButton = styled(SubmitButton)`
  margin-top: 1rem;
`;

const InputLabel = styled(Label)`
  margin-top: 1rem;
  display: block;
`;

const LoginForm = styled.form`
  margin-bottom: 1rem;
`;

const LoginInput = styled(Input)`
  width: 22rem;
`;


class UpdatePassword extends Component {
  state = {
    password: '',
    newPassword: '',
    newPasswordConfirm: '',
    error: null
  }

  reAuth = async() => {
    console.log(auth);
    const creds = firebase.auth.EmailAuthProvider.credential(this.props.user.email, this.state.password);
    return auth.currentUser.reauthenticateAndRetrieveDataWithCredential(creds);
  }

  updatePassword = async(event) => {
    event.preventDefault();
    if(this.state.newPassword !== this.state.newPasswordConfirm) {
      this.setState({error: 'Passwords must match.'});
    } else {
      await this.reAuth();
      this.props.user.updatePassword(this.state.newPassword).then(() => {
        toast.success("Password Updated");
        this.props.history.push('/');
      }).catch((error) => {
        toast.error(error);
      });
    }
  }

  handleChange = (name, event) => {
    this.setState({[name]: event.target.value});
  }

  render() {
    return (
      <UpdatePasswordWrapper>
        <LoginForm>
          <div>
            <InputLabel>Email:</InputLabel>
            <LoginInput type="text" value={this.props.user.email} readOnly />
          </div>
          <div>
            <InputLabel>Current Password:</InputLabel>
            <LoginInput type="password" value={this.state.password} onChange={(e) => this.handleChange('password', e)} />
          </div>
          <div>
            <InputLabel>New Password:</InputLabel>
            <LoginInput type="password" value={this.state.newPassword} onChange={(e) => this.handleChange('newPassword', e)} />
          </div>
          <div>
            <InputLabel>New Password Confirmation:</InputLabel>
            <LoginInput type="password" value={this.state.newPasswordConfirm} onChange={(e) => this.handleChange('newPasswordConfirm', e)} />
          </div>
          { this.state.error && <div>{this.state.error}</div>}
          <div>
            <LoginButton type="submit" value="Submit" onClick={this.updatePassword} />
          </div>
        </LoginForm>
        <Link to="/">Back</Link>
      </UpdatePasswordWrapper>
    );
  }
}

export default withRouter(UpdatePassword);
