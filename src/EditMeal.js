import React, { Component } from 'react';
import _ from 'lodash';
import './App.css';
import firebase from './firebase.js';
import { withRouter} from 'react-router-dom';
import { WithContext as ReactTags } from 'react-tag-input';
import styled from 'styled-components';
import { toast } from 'react-toastify';
import AddItemModal from './AddItemModal';
import {
  Input,
  Select,
  Button,
  ButtonLink,
  Table
} from './styledComponents';

const Header = styled.div`
  width: 100%;
  display: flex;
  justify-content: right;
  align-items: center;
  flex-direction: row;
  border-bottom: 0.1rem solid black;
  padding-bottom: 1rem;
  margin-bottom: 1rem;
`;

const MealNameInput = styled(Input)`
  width: 100%;
`;

const ItemNameInput = styled(Input)`
  width: 100%;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

const QuantityInput = styled(Input)`
  width: 4rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

const UnitsSelect = styled(Select)`
  width: 5rem;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

const NameTd = styled.td`
  border-bottom: none !important;
  padding-bottom: 0 !important;
`;

const RemoveButton = styled(Button)`
  height: 1rem;
  width: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  float: right;
  padding: 0;
  border: none;
`;

const SaveButton = styled(Button)`
  margin-right: 1rem;
  margin-bottom: 1rem;
`;

const DeleteButton = styled(Button)`
  margin-bottom: 1rem;
`;

class EditMeal extends Component {
  constructor(props) {
    super(props);

    let meal = {};

    if(props.new) {
      meal = {
        name: 'new meal',
        items: []
      };
    }

    this.state = {
      meal,
      tab: 'SEARCH'
    };
  }

  componentDidMount() {
    if(this.props.userId) {
      this.loadMeal();
    }
  }

  getMealRef = () => {
    return firebase.database().ref(`users/${this.props.userId}/meals/${this.props.match.params.id}`);
  }

  loadMeal = () => {
    if(!this.props.new) {
      const id = this.props.match.params.id;
      this.getMealRef().once('value', (snapshot) => {
        const meal = snapshot.val();
        if(meal) {
          meal.id = id;
          this.setState({meal}, this.updateLastViewed);
        }
      });
    }
  }

  updateLastViewed = () => {
    if(!this.props.new) {
      this.getMealRef().set({...this.state.meal, lastViewedTimestamp: firebase.database.ServerValue.TIMESTAMP});
    }
  }

  handleItemNameChange = (value, id) => {
    const newFoodItem = _.map(this.state.meal.items, (foodItem) => {
      if(foodItem.id === id) {
        return {...foodItem, name: value};
      }
      return foodItem;
    });
    this.setState({meal: {...this.state.meal, items: newFoodItem}});
  }

  updateQuantity = (value, id) => {
    const newFoodItem = _.map(this.state.meal.items, (foodItem) => {
      if(foodItem.id === id) {
        return {...foodItem, count: value};
      }
      return foodItem;
    });
    this.setState({meal: {...this.state.meal, items: newFoodItem}});
  }

  handleUnitChange = (event, id) => {
    const newFoodItem = _.map(this.state.meal.items, (foodItem) => {
      if(foodItem.id === id) {
        return {...foodItem, units: event.target.value};
      }
      return foodItem;
    });
    this.setState({meal: {...this.state.meal, items: newFoodItem}});
  }

  getTotal = (nutrient) => {
    return _.reduce(this.state.meal.items, (sum, foodItem) => {
      return (foodItem.count * foodItem.measures[foodItem.units][nutrient]) + sum;
    }, 0);
  }

  delete = (id) => {
    const newDetails = _.reject(this.state.meal.items, {id: id});
    this.setState({meal: {...this.state.meal, items: newDetails}});
  }

  saveMeal = () => {
    let meal = {...this.state.meal, lastUpdatedTimestamp: firebase.database.ServerValue.TIMESTAMP};
    if(this.props.new) {
      meal = {...this.state.meal, createdTimestamp: firebase.database.ServerValue.TIMESTAMP};
      this.getMealRef().push(meal);
      toast.success("Meal Created");
      this.props.history.push('/');
    } else {
      this.getMealRef().set(meal);
      toast.success("Meal Saved");
    }
  }

  removeMeal = () => {
    const mealRef = firebase.database().ref(`users/${this.props.userId}/meals/${this.state.meal.id}`);
    mealRef.remove().then(() => {
      toast.success("Meal Removed");
      this.props.history.push('/');
    });
  }

  onChangeName = (event) => {
    this.setState({meal: {...this.state.meal, name: event.target.value}});
  }

  onSelectItem = (item) => {
    this.setState({meal: {...this.state.meal, items: [...this.state.meal.items, item]}});
  }

  handleDeleteTag = (i) => {
    const tags = this.state.meal.tags || [];
    const newTags = tags.filter((tag, index) => index !== i)
    this.setState({meal: {...this.state.meal, tags: newTags}});
  }

  handleAdditionTag = (tag) => {
    const tags = this.state.meal.tags || [];
    const newTags = [...tags, tag]
    this.setState({meal: {...this.state.meal, tags: newTags}});
  }

  render() {
    return (
      <div>
        <Header>
          <ButtonLink to="/">Home</ButtonLink>
        </Header>
        <AddItemModal onSelectItem={this.onSelectItem} foodGroups={this.props.foodGroups} />
        <SaveButton onClick={this.saveMeal}>Save Meal</SaveButton>
        { !this.props.new && <DeleteButton onClick={this.removeMeal}>Delete Meal</DeleteButton> }
        <Table>
          <thead>
            <tr>
              <th colSpan="6">
                <MealNameInput type="text" onChange={this.onChangeName} value={this.state.meal.name || ''} />
              </th>
            </tr>
            <tr>
              <th colSpan="6">
                <ReactTags
                  tags={this.state.meal.tags}
                  handleDelete={this.handleDeleteTag}
                  handleAddition={this.handleAdditionTag}
                  allowDragDrop={false}
                  autofocus={false}
                />
              </th>
            </tr>
            <tr>
              <th>
                Units
              </th>
              <th>
                Qty
              </th>
              <th>
                Cal
              </th>
              <th>
                F
              </th>
              <th>
                C
              </th>
              <th>
                P
              </th>
            </tr>
          </thead>
          <tbody>
            {
              _.map(this.state.meal.items, (foodItem) => {
                return (
                  <React.Fragment key={foodItem.id}>
                    <tr>
                      <NameTd colSpan="5">
                      <ItemNameInput type="text" onChange={(e) => this.handleItemNameChange(e.target.value, foodItem.id)} value={foodItem.name || ''} />
                      </NameTd>
                      <NameTd>
                        <RemoveButton onClick={() => this.delete(foodItem.id)}>X</RemoveButton>
                      </NameTd>
                    </tr>
                    <tr>
                      <td>
                        <UnitsSelect onChange={(e) => this.handleUnitChange(e, foodItem.id)}>
                          { _.map(foodItem.measures, (v, unit) => {
                              return (<option key={unit} value={unit}>{unit}</option>);
                            })
                          }
                        </UnitsSelect>
                      </td>
                      <td><QuantityInput value={foodItem.count} type="number" onChange={(e) => this.updateQuantity(e.target.value, foodItem.id)}/></td>
                      <td>{_.round(foodItem.measures[foodItem.units].calories * foodItem.count)}</td>
                      <td>{_.round(foodItem.measures[foodItem.units].fat * foodItem.count)}</td>
                      <td>{_.round(foodItem.measures[foodItem.units].carbohydrates * foodItem.count)}</td>
                      <td>{_.round(foodItem.measures[foodItem.units].protein * foodItem.count)}</td>
                    </tr>
                  </React.Fragment>
                )
              })
            }
          </tbody>
          <tfoot>
              <tr>
                <td>
                  Total
                </td>
                <td>
                </td>
                <td>
                  {_.round(this.getTotal('calories'))}
                </td>
                <td>
                  {_.round(this.getTotal('fat'))}
                </td>
                <td>
                  {_.round(this.getTotal('carbohydrates'))}
                </td>
                <td>
                  {_.round(this.getTotal('protein'))}
                </td>
              </tr>
            </tfoot>
        </Table>
      </div>
    );
  }
}

export default withRouter(EditMeal);
