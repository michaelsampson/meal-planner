import React, { Component } from 'react';
import './App.css';
import { auth } from './firebase.js';
import { BrowserRouter as Router, Route } from "react-router-dom";
import _ from 'lodash';
import styled from 'styled-components';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { fetchWrapper } from './api';
import Meals from './Meals';
import EditMeal from './EditMeal';
import SignUp from './SignUp';
import Login from './Login';
import UpdatePassword from './UpdatePassword';
import ForgotPassword from './ForgotPassword';

const API_KEY = 'FzYugfLhT6MkSzisA5mGeB0JpQiryNMzPv7N4mqo';

const AppWrapper = styled.div`
  margin: 0 auto;
  padding: 1rem 0 10rem 0;
  width: 24rem;
`;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      loading: true,
      groups: []
    }

    const fontSize = _.min([document.documentElement.clientWidth / 25, 28]);
    document.documentElement.style.fontSize = `${fontSize}px`;
  }

  componentDidMount() {
    this.getFoodGroups();
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user, loading: false });
      } else {
        this.setState({ loading: false });
        if(window.location.pathname !== '/login' && window.location.pathname !== '/signup' && window.location.pathname !== '/forgotPassword') {
          window.location = '/login';
        }
      }
    });
  }

  getFoodGroups = async () => {
    const url = `https://api.nal.usda.gov/ndb/list?max=100&format=json&lt=g&sort=n&api_key=${API_KEY}`
    const results = await fetchWrapper(url, 'GET')
    const groups = _.get(results, 'list.item', []);
    groups.unshift({id: 'ALL', name: 'ALL'});
    this.setState({groups});
  }

  render() {
    const userId = _.get(this.state, 'user.uid');
    return (
      <Router>
        { !this.state.loading &&
          <AppWrapper>
            <Route path="/signup/" component={SignUp} />
            <Route path="/login/" component={Login} />
            <Route path="/forgotPassword/" component={ForgotPassword} />
            <Route path="/updatePassword/" component={() => <UpdatePassword new={true} user={this.state.user} />} />
            <Route path="/new" exact component={() => <EditMeal new={true} userId={userId} foodGroups={this.state.groups} />} />
            <Route path="/edit/:id" exact component={() => <EditMeal userId={userId} foodGroups={this.state.groups} />} />
            <Route path="/" exact component={() => <Meals userId={userId} />} />
            <ToastContainer position='top-center' />
          </AppWrapper>
        }
      </Router>
    );
  }
}

export default App;
