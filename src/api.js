export const fetchWrapper = async (url, method, body) => {
  const config = {
    method: method,
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  };

  if(body) {
    config.body = JSON.stringify(body);
  }

  return fetch(url, config).then((response) => {
    const contentType = response.headers.get("content-type");

    if(contentType && (contentType.includes("application/json") || contentType.includes("text/json"))) {
      return response.json();
    }

    throw new TypeError("Non JSON response from meet creation api.");
  });
};
