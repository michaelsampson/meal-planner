import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { auth } from './firebase.js';
import styled from 'styled-components';

import {
  Input,
  Label,
  SubmitButton
} from './styledComponents';

const LoginWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const LoginButton = styled(SubmitButton)`
  margin-top: 1rem;
`;

const InputLabel = styled(Label)`
  margin-top: 1rem;
  display: block;
`;

const LoginForm = styled.form`
  margin-bottom: 1rem;
`;

const LoginInput = styled(Input)`
  width: 22rem;
`;


class Login extends Component {
  state = {
    email: '',
    password: '',
    error: null
  }

  login = (event) => {
    event.preventDefault();
    const {email, password} = this.state;
    auth.signInWithEmailAndPassword(email, password).then(() => {
      this.props.history.push('/');
    }).catch((error) => {
      console.error(error);
      this.setState({error: error.message});
    });
  }

  handleChange = (name, event) => {
    this.setState({[name]: event.target.value});
  }

  render() {
    return (
      <LoginWrapper>
        <LoginForm>
          <div>
            <InputLabel>Email:</InputLabel>
            <LoginInput type="text" value={this.state.email} onChange={(e) => this.handleChange('email', e)} />
          </div>
          <div>
            <InputLabel>Password:</InputLabel>
            <LoginInput type="password" value={this.state.password} onChange={(e) => this.handleChange('password', e)} />
          </div>
          { this.state.error && <div>{this.state.error}</div>}
          <div>
            <LoginButton type="submit" value="Submit" onClick={this.login} />
          </div>
        </LoginForm>
        <br/>
        <Link to="/signup">Sign Up</Link><br/>
        <Link to="/forgotPassword">Forgot Password</Link>
      </LoginWrapper>
    );
  }
}

export default Login;
