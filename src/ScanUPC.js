import React, { Component } from 'react';
import _ from 'lodash';
import { fetchWrapper } from './api';
import styled from 'styled-components';
import uuidv4 from 'uuid/v4';

import BarcodeScanner from './BarcodeScanner';

import {
  Button
} from './styledComponents';

const ScanUPCWrapper = styled.div`
  height: 30rem;
`;

const ResetButton = styled(Button)`
  margin-top: 1rem;
`;

const AcceptButton = styled(Button)`
  margin-left: 1rem;
  margin-top: 1rem;
`;

const API_KEY = 'FzYugfLhT6MkSzisA5mGeB0JpQiryNMzPv7N4mqo';

class ScanUPC extends Component {
  constructor(props) {
    super(props);

    this.state = {
      code: '',
      error: '',
      newFoodItem: null
    }
  }

  onDetected = (data) => {
    if(data.codeResult.code && !this.state.code) {
      console.log('onDetected', data);
      this.setState({code: data.codeResult.code}, this.search);
    }
  }

  getGramNutrientCountFromResults = (results, dataType) => {
    const data = _.find(_.get(results, 'report.food.nutrients', {}), {name: dataType});
    const valuePer100Grams = _.get(data, 'value', 0);
    return valuePer100Grams / 100;
  }

  getNutrientCountFromMeasure = (results, measureLabel, dataType) => {
    const data = _.find(_.get(results, 'report.food.nutrients', {}), {name: dataType});
    const measureData = _.find(data.measures, {label: measureLabel});
    if(!measureData) {
      return 0;
    }
    return _.toNumber(measureData.value) / measureData.qty;
  }

  fetchDetails = async (ndbno) => {
    const url = `https://api.nal.usda.gov/ndb/reports/?ndbno=${ndbno}&type=b&format=json&api_key=${API_KEY}`
    const results = await fetchWrapper(url, 'GET');

    const newFoodItem = {
      id: uuidv4(),
      ndbno: ndbno,
      name: _.get(results, 'report.food.name', ''),
      measures: {
        g: {
          calories: this.getGramNutrientCountFromResults(results, 'Energy'),
          fat: this.getGramNutrientCountFromResults(results, 'Total lipid (fat)'),
          carbohydrates: this.getGramNutrientCountFromResults(results, 'Carbohydrate, by difference'),
          protein: this.getGramNutrientCountFromResults(results, 'Protein')
        },
      },
      units: 'g',
      count: 100
    };

    _.each(results.report.food.nutrients[0].measures, (measure) => {
      if(measure) {
        newFoodItem.measures[this.cleanMeasureLable(measure.label)] = {
          calories: this.getNutrientCountFromMeasure(results, measure.label, 'Energy'),
          fat: this.getNutrientCountFromMeasure(results, measure.label, 'Total lipid (fat)'),
          carbohydrates: this.getNutrientCountFromMeasure(results, measure.label, 'Carbohydrate, by difference'),
          protein: this.getNutrientCountFromMeasure(results, measure.label, 'Protein')
        };
      }
    });

    this.setState({newFoodItem, error: ''});
  };

  cleanMeasureLable = (measureLabel) => {
    return _.replace(measureLabel, '/', ' per ');
  }

  search = async () => {
    let url = `https://api.nal.usda.gov/ndb/search/?sort=r&format=json&q=${this.state.code}&max=500&offset=0&api_key=${API_KEY}`;
    const results = await fetchWrapper(url, 'GET')
    const items = _.get(results, 'list.item', []);
    const item = _.first(items);
    if(item) {
      this.fetchDetails(item.ndbno)
    } else {
      this.setState({error: 'Could not find item'});
    }
  }

  reset = () => {
    this.setState({newFoodItem: null, code: '', error: ''});
  }

  accept = () => {
    this.props.onSelect(this.state.newFoodItem);
    this.reset();
  }

  render() {
    return (
      <ScanUPCWrapper>
        { !this.state.code &&
          <BarcodeScanner onScan={this.onDetected} />
        }
        { this.state.code &&
          <div>
            <div>{this.state.code}</div>
            { this.state.error &&
              <div>
                {this.state.error}
              </div>
            }
            <div><label>Name:</label>{_.get(this.state, 'newFoodItem.name')}</div>
            <div><label>Fat per gram:</label>{_.get(this.state, 'newFoodItem.measures.g.fat')}</div>
            <div><label>Carbs per gram:</label>{_.get(this.state, 'newFoodItem.measures.g.carbohydrates')}</div>
            <div><label>Protein per gram:</label>{_.get(this.state, 'newFoodItem.measures.g.protein')}</div>
            <ResetButton onClick={this.reset}>Reset</ResetButton>
            { !this.state.error && <AcceptButton onClick={this.accept}>Accept</AcceptButton> }
          </div>
        }
      </ScanUPCWrapper>
    );
  }
}

export default ScanUPC;
